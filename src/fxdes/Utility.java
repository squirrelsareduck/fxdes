/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxdes;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrew
 */
class Utility
{

    public byte[] performPermutations(byte[] originalByteArray, String inputPermutationTextFile)
    {
        byte[] permutedText;
        ArrayList<Integer> newlyPermutedLocations = new ArrayList<>();

        Scanner scanner = scanInTextFile(inputPermutationTextFile);

        while (scanner.hasNextInt())
        {
            int value = scanner.nextInt();
            newlyPermutedLocations.add(value - 1); // Positions now between 0 and 63
        }

        int quantityOfNecessaryBytes = (int) Math.ceil(newlyPermutedLocations.size() / 8.0);
        permutedText = new byte[quantityOfNecessaryBytes];
     
        for (int destinationBitIndex = 0; destinationBitIndex < newlyPermutedLocations.size(); destinationBitIndex++)
        {
            // Rearrange permutedText
            int originBitIndex = (int) newlyPermutedLocations.get(destinationBitIndex);

            if (getBit(originalByteArray, originBitIndex) == 1)
            {
                setBit(permutedText, destinationBitIndex);
            }
            else
            {
                clearBit(permutedText, destinationBitIndex);
            }
        }

        return permutedText;
    }

    // Could consider using BitSets to accomplish this, might be faster.
    // Yet, very frustrated with BitSets at the moment, as they don't maintain
    // preceeding 0s, and I don't want to debug them anymore than I have to
    public int getBit(byte[] arrayOfInterest, int index)
    {
        // Note: index is 0 based, hence from 0 to 7
        int byteIndexOfInterest = (int) Math.floor(index / 8.0);
        int indexWithinByte = index % 8;
        byte byteOfInterest = arrayOfInterest[byteIndexOfInterest];

        // We  now have the byte of interest.
        // Next, to determine if the bit at index indexWithinByte is set,
        // we shift the bits to the right and mask the result with an "and 1"
        int quantityToShiftRight = 7 - indexWithinByte;
        int bitValue = byteOfInterest >> quantityToShiftRight & 0x0001;
        return bitValue;
    }

    public void setBit(byte[] arrayOfInterest, int index)
    {
        // Note: index is 0 based, hence from 0 to 7
        int byteIndexOfInterest = (int) Math.floor(index / 8);
        int indexWithinByte = index % 8;
        byte byteOfInterest = arrayOfInterest[byteIndexOfInterest];

        // We need to OR the original array with a mask, according to the index
        // We use the inclusive OR | instead of the XOR ^
        byte newByte = (byte) ((0x0001 << (7 - indexWithinByte)) | byteOfInterest);
        arrayOfInterest[byteIndexOfInterest] = newByte;
    }

    public void clearBit(byte[] arrayOfInterest, int index)
    {
        // Note: index is 0 based, hence from 0 to 7
        int byteIndexOfInterest = (int) Math.floor(index / 8);
        int indexWithinByte = index % 8;
        byte byteOfInterest = arrayOfInterest[byteIndexOfInterest];

        // 0xFF7F = 1111 1111 0111 1111
        // Shift that 0 to the necessary position,
        // AND command asserts 0 into newByte.
        // AND command clears out unnecessary leading 1s
        byte newByte = (byte) (((0xFF7F >> indexWithinByte) & byteOfInterest) & 0x00FF);
        arrayOfInterest[byteIndexOfInterest] = newByte;
    }

    byte[] bytes28BitsRotateLeft(byte[] previousBytes, int quantityOfBitsToRotate)
    {
        // Note: quantityOfBitsToRotate hasn't been implemented yet.  Currently
        // perma-set to 1

        // Important!!!!!!! only left-most (first) 28 bits are considered relevant!!!!
        // All else are ignored.
        byte[] newlyShiftedByteArray = new byte[4];
        for (int copyIndex = 0; copyIndex < 28; copyIndex++)
        {
            if (copyIndex == 0)
            {
                if (getBit(previousBytes, copyIndex) == 1)
                {
                    setBit(newlyShiftedByteArray, 27);
                }
                else
                {
                    clearBit(newlyShiftedByteArray, 27);
                }
            }
            else
            {
                if (getBit(previousBytes, copyIndex) == 1)
                {
                    setBit(newlyShiftedByteArray, copyIndex - 1);
                }
                else
                {
                    clearBit(newlyShiftedByteArray, copyIndex - 1);
                }
            }
            clearBit(newlyShiftedByteArray, 28);
            clearBit(newlyShiftedByteArray, 29);
            clearBit(newlyShiftedByteArray, 30);
            clearBit(newlyShiftedByteArray, 31);
        }
        return newlyShiftedByteArray;
    }

    byte[] joinBitsInByteArrays(byte[] firstInput, byte[] secondInput, int firstLength, int secondLength)
    {
        int quantityOfJoinedBytes = (int) Math.ceil((firstLength + secondLength) / 8.0);
        byte[] out = new byte[quantityOfJoinedBytes];
        int newIndex = 0;
        for (int subIndex = 0; subIndex < firstLength; subIndex++)
        {
            int bitValue = getBit(firstInput, subIndex);
            if (bitValue == 1)
            {
                setBit(out, newIndex);
            }
            else
            {
                clearBit(out, newIndex);
            }
            newIndex++;
        }
        for (int subIndex = 0; subIndex < secondLength; subIndex++)
        {
            int bitValue = getBit(secondInput, subIndex);
            if (bitValue == 1)
            {
                setBit(out, newIndex);
            }
            else
            {
                clearBit(out, newIndex);
            }
            newIndex++;
        }
        return out;
    }

    Scanner scanInTextFile(String textFile)
    {
        Scanner scanner = null;
        try
        {
            scanner = new Scanner(this.getClass().getResourceAsStream(textFile));
        } catch (Exception ex)
        {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return scanner;
    }
}