package fxdes;

/**
 *
 * @author Andrew
 */
class KeyGenerator
{

    Utility utility;

    KeyGenerator()
    {
        utility = new Utility();
    }

    public byte[] performPC1(byte[] key56Bits)
    {
        return utility.performPermutations(key56Bits, "pc1.txt");
    }

    public byte[][] generateSubKeys(byte[] afterPC1)
    {
        //[index][] denotes each iteration of the subkey generation.
        //[][otherIndex] denotes the 28 bits of interest
        byte[][] leftTransformationInputs = generateTransformationInputs(afterPC1, "left");
        byte[][] rightTransformationInputs = generateTransformationInputs(afterPC1, "right");
        // Note: the rotations are already performed in the above functions.

        byte[][] joinedTransformationInputs = joinTransformationInputs(leftTransformationInputs, rightTransformationInputs);

        byte[][] transformationOutputs = generateTransformationOutputs(joinedTransformationInputs);
        return transformationOutputs;
    }

    // Stores bits in a byte array.  If bits don't fill the array, the remaining
    // bits are 0 by default, as in #########0000
    // startIndex is 0 based
    byte[] extractBits(byte[] byteToExtractFrom, int startIndex, int length)
    {
        int numOfBytes = (int) Math.ceil(length / 8.0);
        
        byte[] result = new byte[numOfBytes];
        for (int index = 0; index < length; index++)
        {
            int valueOfInterest = utility.getBit(byteToExtractFrom, startIndex + index);
            if (valueOfInterest == 1)
            {
                utility.setBit(result, index);
            }
            else
            {
                utility.clearBit(result, index);
            }
        }
        return result;
    }

    byte[][] generateTransformationInputs(byte[] afterPC1, String leftOrRight)
    {
        byte[][] result = new byte[16][4];
        byte[] originalHalfOfInterest;
        if (leftOrRight.equals("left"))
        {
            originalHalfOfInterest = extractBits(afterPC1, 0, 28);
        }
        else
        {
            originalHalfOfInterest = extractBits(afterPC1, 28, 28);
        }

        byte[] previousBytes = originalHalfOfInterest;

        for (int index = 0; index <= 15; index++)
        {
            byte[] newlyShifted28;

            // Iterations: 0, 1, 8, 15: left rotation 1 bit
            if (index == 0 || index == 1 || index == 8 || index == 15)
            {
                newlyShifted28 = utility.bytes28BitsRotateLeft(previousBytes, 1);
            }
            // All else: left rotation 2 bits
            else
            {
                // TODO: improve performance of this by directly shifting 2 bits,
                // instead of just shifting by 1 bit, twice
                byte[] partiallyShiftedLeft28 = utility.bytes28BitsRotateLeft(previousBytes, 1);
                newlyShifted28 = utility.bytes28BitsRotateLeft(partiallyShiftedLeft28, 1);
            }

            previousBytes = (byte[]) newlyShifted28.clone();
            result[index] = previousBytes;
        }
        return result;
    }

    byte[][] generateTransformationOutputs(byte[][] joinedTransformationInputs)
    {
        byte[][] result = new byte[16][6];

        // Apply PC2 to each of the 16 byte arrays
        for (int index = 0; index < 16; index++)
        {
            result[index] = utility.performPermutations(joinedTransformationInputs[index], "pc2.txt");
        }
        return result;
    }

    byte[][] joinTransformationInputs(byte[][] leftTransformationInputs, byte[][] rightTransformationInputs)
    {
        // Changing this to 7 didn't effect output.
        // I think this is ok.  the byte[someIndex][] is created anew in the loop
        // hence the 2nd index during instantiation isn't completely relevant.
        byte[][] joinedTransformationInputs = new byte[16][7];
        for (int index = 0; index < 16; index++)
        {
            joinedTransformationInputs[index] = utility.joinBitsInByteArrays(leftTransformationInputs[index], rightTransformationInputs[index], 28, 28);
        }
        return joinedTransformationInputs;
    }
}