package fxdes;

import java.util.Scanner;

/**
 *
 * @author Andrew
 */
class InputHandler
{

    Scanner scanner;
    Utility utility;

    InputHandler()
    {
        scanner = new Scanner(System.in);
        utility = new Utility();
    }

    String promptUserForInputText(boolean isATestRun, String mode)
    {
        printPromptToScreen(mode);
        boolean awaitingValidInput = true;
        while (awaitingValidInput == true)
        {
            String methodOfPlainTextRetrieval;
            if (isATestRun)
            {
                methodOfPlainTextRetrieval = "TF";
            }
            else
            {
                methodOfPlainTextRetrieval = scanner.nextLine();
            }
            if (methodOfPlainTextRetrieval.equalsIgnoreCase("TF"))
            {

                System.out.println("\nMake sure that your input text is in the ");
                System.out.println("file 'inputPlainText.txt' that is included in the ");
                System.out.println("project directory, then \n\npress enter to continue");
                if (!isATestRun)
                {
                    scanner.nextLine();
                }
                Scanner scannerTFinput = utility.scanInTextFile("inputPlainText.txt");
                String result = "";
                while (scannerTFinput.hasNextLine())
                {
                    result += scannerTFinput.nextLine();
                }
                return result;
            }
            else if (methodOfPlainTextRetrieval.equalsIgnoreCase("CLI"))
            {
                awaitingValidInput = false;
                if (mode.equalsIgnoreCase("encrypt"))
                {
                    System.out.println("Please type the text that you wish to encrypt using DES:");
                }
                else
                {
                    System.out.println("Please type the encrypted text (in hex format)");
                    System.out.println("that you wish to decrypt using DES.  Please");
                    System.out.println("exclude all spaces: ");
                }
                String plainText = scanner.nextLine();
                if (plainText.equals(""))
                {
                    // Do error handling
                }
                else
                {
                    return plainText;
                }
            }
            else
            {
                // Tell user they typed an invalid input format.
                System.out.println("Invalid selection.\n\n");
                printPromptToScreen(mode);
            }
        }
        return null;
    }

    private void printPromptToScreen(String mode)
    {
        if (mode.equalsIgnoreCase("encrypt"))
        {
            System.out.println("\nDo you want to encrypt from a text file or command line input?");
        }
        else
        {
            System.out.println("\nDo you want to decrypt from a text file or command line input?");
        }
        System.out.println("[Note: Text File mode requires that you re-compile");
        System.out.println("the code after editing the text file]\n");
        System.out.print("Type TF for textfile or CLI for command line input: ");
    }

    private void printKeyPromptToScreen()
    {
        System.out.println("Please type an 8-letter key: ");
    }

    String promptUserForKey(boolean isATestRun)
    {
        printKeyPromptToScreen();
        boolean awaitingValidInput = true;
        while (awaitingValidInput == true)
        {
            String entry;
            if (isATestRun)
            {
                return "geronimo";
            }
            else
            {
                entry = scanner.nextLine();
            }
            if (entry.length() == 8)
            {
                return entry;
            }
            else
            {
                System.out.println("\nTry again. Please enter an 8 digit key: ");
            }
        }
        return null;
    }

    private void printModePromptToScreen()
    {
        System.out.println("Please type 'Encryption', 'Decryption', or 'Both': ");
    }

    String promptUserForEncryptionOrDecryptionMode(boolean isATestRun)
    {
        printModePromptToScreen();
        boolean awaitingValidInput = true;
        while (awaitingValidInput == true)
        {
            String entry;
            if (isATestRun)
            {
                return "both";
            }
            else
            {
                entry = scanner.nextLine();
            }
            if (entry.equalsIgnoreCase("encryption") || entry.equalsIgnoreCase("decryption")
                    || entry.equalsIgnoreCase("both"))
            {
                return entry;
            }
            else
            {
                System.out.println("\nTry again. Please type\n'Encryption', 'Decryption', or 'Both': ");
            }
        }
        return null;
    }
}
