/*
 * Andrew Elek
 * Applied Cryptography
 */
package fxdes;

import java.util.Arrays;
import java.util.Scanner;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author Andrew
 */
public class DESencryption
{

    static String key64bitString = "geronimo";
    static Utility utility = new Utility();
    static InputHandler inputHandler = new InputHandler();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        System.out.println("JFXV: " + com.sun.javafx.runtime.VersionInfo.getRuntimeVersion());
       
         //   System.out.println("SUPER IMPORTANT! " + args.length);
        for (int index = 0; index < args.length; index++)
        {
            System.out.println(args[index]);
        }
        String isATestRun = "";
        boolean isATestRunBoolean = false;
        if (args.length > 0)
        {
            isATestRun = args[0];
        }
        if (!isATestRun.equals(""))
        {
            isATestRunBoolean = true;
        }

        String mode = inputHandler.promptUserForEncryptionOrDecryptionMode(isATestRunBoolean);
        byte[] cipherText = null;
        if (mode.equalsIgnoreCase("encryption") || mode.equalsIgnoreCase("both"))
        {
            cipherText = encryptOrDecrypt(isATestRunBoolean, "encrypt", null, false, null);
        }

        System.out.println("***************************");
        if (mode.equalsIgnoreCase("decryption") || mode.equalsIgnoreCase("both"))
        {
            System.out.println("Attempting decryption:");
            byte[] plainText = encryptOrDecrypt(isATestRunBoolean, "decrypt", cipherText, false, null);
        }
    }

    public static String externalEncryption(String plainText, String key)
    {
        return Hex.encodeHexString(encryptOrDecrypt(false, "encrypt", plainText.getBytes(), true, key));
    }

    public static String externalDecryption(String cipherText, String key)
    {
        byte[] cipherTextInBytes = DatatypeConverter.parseHexBinary(cipherText);
        return new String(encryptOrDecrypt(false, "decrypt", cipherTextInBytes, true, key));
    }

    static byte[] encryptOrDecrypt(boolean isATestRun, String encryptOrDecrypt, byte[] cipherText, boolean hardPushThroughOfParameter, String key)
    {
        String originalText = null;
        byte[] originalTextInBytes;
        if (encryptOrDecrypt.equalsIgnoreCase("decrypt"))
        {
            String hexToDecrypt = null;
            // TODO
            if (cipherText == null)
            {
                hexToDecrypt = inputHandler.promptUserForInputText(isATestRun, "decrypt");
                while (hexToDecrypt.length() % 16 != 0)
                { // error handling
                    System.out.println("\nInput was invalid for decryption. Try again:");
                    hexToDecrypt = inputHandler.promptUserForInputText(isATestRun, "decrypt");
                }
                // Convert the hex into a byte array
                cipherText = DatatypeConverter.parseHexBinary(hexToDecrypt);
            }

            originalTextInBytes = cipherText;
            if (hardPushThroughOfParameter)
            {
                key64bitString = key;
            }
            else
            {
                key64bitString = inputHandler.promptUserForKey(isATestRun);
            }
        }
        else
        {
            // DES acts upon 64 bits of plainText with 56 bits of key, and generates
            // 64 bits of cipherText
            if (hardPushThroughOfParameter)
            {
                key64bitString = key;
                originalTextInBytes = cipherText;
            }
            else
            {
                originalText = inputHandler.promptUserForInputText(isATestRun, "encrypt");
                key64bitString = inputHandler.promptUserForKey(isATestRun);
                originalTextInBytes = originalText.getBytes();
            }
        }
        // Hence, DES operates on 8 Bytes of plainText.  So lets break the plainText
        // into sections of 8 Bytes

        System.out.println("OriginalText in Byte form: " + Hex.encodeHexString(originalTextInBytes));
        if (originalText != null)
        {
            System.out.println("OriginalText in Text Form: " + originalText);
        }
        int quantityOfNecessaryIterations = (int) Math.ceil(originalTextInBytes.length / 8.0);
        byte[][] endingBytes = new byte[quantityOfNecessaryIterations][8];

        // This iterates through the DES algorithm as much as required by the given
        // text
        for (int desIteration = 0; desIteration < quantityOfNecessaryIterations; desIteration++)
        {
            int lesserIndex = 8 * desIteration + 0;
            int higherIndex = 8 * desIteration + 8;
            if (higherIndex > originalTextInBytes.length)
            {
                higherIndex = originalTextInBytes.length;
            }

            byte[] atMost8PlainTextBytes = Arrays.copyOfRange(originalTextInBytes, lesserIndex, higherIndex);
            byte[] exactly8PlainTextBytes;

            if (atMost8PlainTextBytes.length < 8)
            {
                exactly8PlainTextBytes = new byte[8];
                int extraBytesRequired = 8 - atMost8PlainTextBytes.length;
                byte aByte = (byte) 0;
                System.arraycopy(atMost8PlainTextBytes, 0, exactly8PlainTextBytes, 0, atMost8PlainTextBytes.length);
                for (int a = atMost8PlainTextBytes.length; a < 8; a++)
                {
                    exactly8PlainTextBytes[a] = aByte;
                }
                //System.out.println("THIS SHOULD THEORETICALLY BE AN 8 BYTE SECTION: " + new BigInteger(exactly8PlainTextBytes).toString(2));
            }
            else
            {
                exactly8PlainTextBytes = atMost8PlainTextBytes;
            }

            byte[] permutedTextInBytes = utility.performPermutations(exactly8PlainTextBytes, "initialPermutation.txt");
            byte[] leftInputToFeistelNetwork = Arrays.copyOfRange(permutedTextInBytes, 0, 4);
            byte[] rightInputToFeistelNetwork = Arrays.copyOfRange(permutedTextInBytes, 4, 8);

            KeyGenerator keyGenerator = new KeyGenerator();
            byte[] key64bits = key64bitString.getBytes();
            byte[] afterPC1 = keyGenerator.performPC1(key64bits);
            byte[][] subKeys = keyGenerator.generateSubKeys(afterPC1);
            if (encryptOrDecrypt.equalsIgnoreCase("decrypt"))
            {
                // Reverse order of subKeys
                byte[][] temp = new byte[16][6];
                for (int index = 0; index < 16; index++)
                {
                    temp[index] = subKeys[15 - index];
                }
                subKeys = temp;
            }

            for (int index = 0; index < 16; index++)
            {
                //System.out.println("doing feistel iteration " + index);
                byte[][] result = performFeistelFunctions(leftInputToFeistelNetwork, rightInputToFeistelNetwork, subKeys[index]);
                leftInputToFeistelNetwork = result[0];
                rightInputToFeistelNetwork = result[1];

            }

            // This can be optimized
            byte[] afterFeistels = utility.joinBitsInByteArrays(rightInputToFeistelNetwork, leftInputToFeistelNetwork, 32, 32);

            byte[] finalCipherTextInByteForm = utility.performPermutations(afterFeistels, "finalPermutation.txt");
            endingBytes[desIteration] = finalCipherTextInByteForm;

        }
        byte[] linearResult = new byte[endingBytes.length * 8];
        for (int index = 0; index < endingBytes.length; index++)
        {
            for (int secondary = 0; secondary < ((endingBytes[index]).length); secondary++)
            {
                linearResult[index * 8 + secondary] = endingBytes[index][secondary];
            }
        }

        if (encryptOrDecrypt.equalsIgnoreCase("encrypt"))
        {
            System.out.println("cipher text in byte form: ");
        }
        else
        {
            System.out.println("plain text in byte form: ");
        }
        for (int index = 0; index < endingBytes.length; index++)
        {
            System.out.println(Hex.encodeHexString(endingBytes[index]));
        }

        if (encryptOrDecrypt.equalsIgnoreCase("encrypt"))
        {
            System.out.println("CipherText in Text Form: ");
        }
        else
        {
            System.out.println("PlainText in Text Form: ");
        }

        String resultString = "";
        for (int index = 0; index < endingBytes.length; index++)
        {
            resultString += new String(endingBytes[index]);
        }
        System.out.println(resultString);
        //System.out.println("DEBUG RESULT: " + Hex.encodeHexString(resultString.getBytes()));
        return linearResult;
    }

    // [0][] is going to be the left output 4 Bytes
    // [1][] is going to be the right output 4 Bytes
    static byte[][] performFeistelFunctions(byte[] leftInputToFeistelNetwork,
            byte[] rightInputToFeistelNetwork, byte[] subKey)
    {
        byte[][] result = new byte[2][4];
        result[0] = rightInputToFeistelNetwork;

        //System.out.println("commencing FeistelFunctions");
        byte[] afterFeistelFfunction = performFfunction(subKey, rightInputToFeistelNetwork);

        // XOR the previous leftInput with the feistel output. result goes to rightoutput        
        for (int index = 0; index < 4; index++)
        {
            result[1][index] = (byte) (((byte) afterFeistelFfunction[index]) ^ ((byte) leftInputToFeistelNetwork[index]));
        }

        return result;
    }

    static byte[] performFfunction(byte[] subKey, byte[] rightInputToFeistelNetwork)
    {
        // Expansion
        byte[] expanded = utility.performPermutations(rightInputToFeistelNetwork, "expansionFunction.txt");

        // XOR with subKey
        byte[] xORed = new byte[6];
        for (int index = 0; index < 6; index++)
        {
            byte xORedValue = (byte) (expanded[index] ^ subKey[index]);
            xORed[index] = xORedValue;
        }

        // sBoxify
        byte[] sBoxed = sBoxify(xORed);

        // Permutate the result of the sBoxes
        byte[] result = utility.performPermutations(sBoxed, "permutation.txt");
        return result;
    }

    /**
     * Each of the 8 sBox attempts take 6 bits input and output 4 bits.
     *
     * Hence, the result is a (32 bit = 4 byte = 8 hex) output.
     *
     * @param xORed
     * @return
     */
    private static byte[] sBoxify(byte[] xORed)
    {
        // int sBoxOutputVessel = 0;
        byte[] sBoxOutputVessel = new byte[8];
        // S-box
        for (int index = 0; index < 8; index++)
        {
            // Calculate row and column to be selected in the S-box
            int startIndex = 6 * index;
            int endIndex = 6 * index + 5;
            int valueOfStartBit = utility.getBit(xORed, startIndex);
            int valueOfEndBit = utility.getBit(xORed, endIndex);
            String rowInBinaryString = Integer.toString(valueOfStartBit) + Integer.toString(valueOfEndBit);
            int rowInIntegerForm = Integer.parseInt(rowInBinaryString, 2);
            //System.out.println("DEBUG ROW " + rowInIntegerForm);

            String columnInBinaryString = "";
            // Now calculate the column
            for (int columnIndexer = startIndex + 1; columnIndexer < endIndex; columnIndexer++)
            {
                int valueOfCurrentBit = utility.getBit(xORed, columnIndexer);
                columnInBinaryString += valueOfCurrentBit + "";
            }
            int columnInIntegerForm = Integer.parseInt(columnInBinaryString, 2);
            //System.out.println("DEBUG COLUMN " + columnInIntegerForm);

            // With the row and column, we calculate the overall position within
            // the S-box
            int position = rowInIntegerForm * 16 + columnInIntegerForm;
            //System.out.println("DEBUG POSITION " + position);

            // Now, we scan in integers up to that position.
            String textFileName = "S" + (index + 1) + ".txt";
            Scanner scanner = utility.scanInTextFile(textFileName);
            int sBoxValue = -1;
            for (int counter = 0; counter <= position; counter++)
            {
                if (scanner.hasNextInt())
                {
                    int value = scanner.nextInt();
                    if (counter == position)
                    {
                        sBoxValue = value;
                    }
                }
            }

            sBoxOutputVessel[index] = (byte) (sBoxValue << 4);

        }
        byte[] temp0 =
        {
            sBoxOutputVessel[0]
        };
        byte[] temp1 =
        {
            sBoxOutputVessel[1]
        };
        byte[] tempJointByteArray = utility.joinBitsInByteArrays(temp0, temp1, 4, 4);

        for (int index = 2; index < 8; index++)
        {
            byte[] valueHolder = tempJointByteArray.clone();
            byte[] byteArrayOfInterest =
            {
                sBoxOutputVessel[index]
            };
            tempJointByteArray = utility.joinBitsInByteArrays(valueHolder, byteArrayOfInterest, 4 + 4 * (index - 1), 4);
        }
        byte[] result = tempJointByteArray;

        return result;
    }
}
