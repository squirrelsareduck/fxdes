/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxdes;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

/**
 *
 * @author Andrew
 */
public class FXMLDocumentController implements Initializable
{

    @FXML
    private RadioButton decryptButton;
    @FXML
    private RadioButton encryptButton;
    @FXML
    private TextField originalTextField;
    @FXML
    private TextField keyField;
    @FXML
    private TextField outputField;
    @FXML
    private Label instructionalLabel;

    @FXML
    private void startEncryptorOrDecryptor(ActionEvent event)
    {
        String output = "";
        if (encryptButton.isSelected())
        {
            output = DESencryption.externalEncryption(originalTextField.getText(), keyField.getText());
        }
        else if (decryptButton.isSelected())
        {
            output = DESencryption.externalDecryption(originalTextField.getText(), keyField.getText());
        }
        outputField.setText(output);
    }

    @FXML
    private void encryptionSelected(ActionEvent event)
    {
        System.out.println("Encryption button clicked");
        decryptButton.setSelected(false);
        instructionalLabel.setText("Type the text you want to encrypt");
    }

    @FXML
    private void decryptionSelected(ActionEvent event)
    {
        System.out.println("Decryption button clicked");
        encryptButton.setSelected(false);
        instructionalLabel.setText("Type the text you want to decrypt");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        keyField.lengthProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
            {
                if (newValue.intValue() > oldValue.intValue())
                {
                    if (keyField.getText().length() > 8)
                    {
                        keyField.setText(keyField.getText().substring(0, 8));
                    }
                }
            }
        });
    }
}
